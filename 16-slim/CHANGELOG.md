# node-16-slim 1.0.0 (2022-12-20)


### Features

* init release ([e1ac76c](https://gitlab.com/beepbeepgo/public/docker/nodejs/commit/e1ac76c87366d2e16e13c76022bd30c2ebc21e89))

# node-16-slim 1.0.0-issue-init.1 (2022-12-20)


### Features

* init release ([2f6391a](https://gitlab.com/beepbeepgo/public/docker/nodejs/commit/2f6391ab010f17aa90becbe889edce285321bbe1))
